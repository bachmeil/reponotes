This is a pair of scripts that allows you to easily add a note to a directory on your computer. I created it because I wanted to add notes about a project to a Git repo, and then view those notes in the browser, without a lot of setup or maintenance.

I imposed the following constraints:

- It has to be trivial to add a new note. The user should only have to call the script and will then be prompted to enter (a small number of pieces) of metadata.
- There has to be support for attachments and comments.
- There has to be support for Markdown, to make it easy to enter and read the text of notes at the command line.
- The note, along with attachments and comments, have to compile to static html files.
- There has to be support for equations and code.
- It has to be simple to customize.
- Everything has to be done with text files, so that it is easy to add to a Git repo, to copy to a USB drive, or email the html to someone else.
- It has to be easy to put on a web server.
- Maintenance has to be minimal. Conversion to html is done by pandoc, an active project, so the scripts should work for years with little or no maintenance.